package fr.uavignon.shuet.tp2.data;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    WineDbHelper data = new WineDbHelper(WineActivity.this); //instancier un objet de la classe WinedDbHelper pour utiliser les méthodes qui sont à l'intérieur

    EditText WineName;
    EditText Region;
    EditText Loc;
    EditText Climate;
    EditText PlantedArea;
    Button save;
    Wine winee = new Wine(); // objet vide

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        // recuperer toutes les vues
        WineName = (findViewById(R.id.wineName));
        Region = (findViewById(R.id.editWineRegion));
        Loc = (findViewById(R.id.editLoc));
        Climate = (findViewById(R.id.editClimate));
        PlantedArea = (findViewById(R.id.editPlantedArea));
        save = findViewById(R.id.button);

        Intent intent = getIntent();

        // verifier si l'intent n'est pas vide -- MAJ --
        if(intent.hasExtra("vin")) {

            // recup l'objet envoyé de la premiere activité
            final Wine wineBis = (Wine) intent.getParcelableExtra("vin");

            // pour remplir les editText
            WineName.setText("");
            WineName.setText(wineBis.getTitle());
            Region.setText(wineBis.getRegion());
            Loc.setText(wineBis.getLocalization());
            Climate.setText(wineBis.getClimate());
            PlantedArea.setText(wineBis.getPlantedArea());


            save.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // recup les champs pour recup les val saisies et les mettre sur l'objet
                    wineBis.setTitle(WineName.getText().toString());
                    wineBis.setRegion(Region.getText().toString());
                    wineBis.setLocalization(Loc.getText().toString());
                    wineBis.setClimate(Climate.getText().toString());
                    wineBis.setPlantedArea(PlantedArea.getText().toString());

                    if(wineBis.getTitle().matches("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("Le nom du vin doit être non vide !  ");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }

                    data.updateWine(wineBis);

                    //cnl.sauvgarde(n);
                    Toast.makeText(WineActivity.this, "Sauvgarde effectuée !", Toast.LENGTH_LONG).show();
                    Intent intent2 = new Intent(WineActivity.this, MainActivity.class);
                    startActivity(intent2);
                    finish();
                }
            });
        }
        // Si l'objet est vide -- ajout --
        else {
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // il recup les champs pour les mettre dans l'objet vide winee
                    winee.setTitle(WineName.getText().toString());
                    winee.setRegion(Region.getText().toString());
                    winee.setLocalization(Loc.getText().toString());
                    winee.setClimate(Climate.getText().toString());
                    winee.setPlantedArea(PlantedArea.getText().toString());

                    if(winee.getTitle().matches("") ){
                        AlertDialog.Builder builder = new AlertDialog.Builder(WineActivity.this);
                        builder.setMessage("Le nom du vin doit être non vide ! ");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }
                    else
                        /*if (winee.getTitle().equals(WineName.getText().toString())){

                            Toast.makeText(WineActivity.this, "Vous avez ce vin en double, buvez avec modération !", Toast.LENGTH_LONG).show();
                            Intent intent2 = new Intent(WineActivity.this, MainActivity.class);
                        }*/
                    {
                        data.addWine(winee);
                        Toast.makeText(WineActivity.this, "Votre vin a bien été ajotée à votre cave !", Toast.LENGTH_LONG).show();
                        Intent intent2 = new Intent(WineActivity.this, MainActivity.class);
                        startActivity(intent2);
                        finish();
                    }
                }
            });
        }
        }
    }
