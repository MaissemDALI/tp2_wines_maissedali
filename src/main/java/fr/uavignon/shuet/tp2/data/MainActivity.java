package fr.uavignon.shuet.tp2.data;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    WineDbHelper data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


       // adapter.notifyDataSetChanged();


        // add bouton

        FloatingActionButton add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();*/

                Intent intent2 = new Intent(MainActivity.this, WineActivity.class);
                startActivity(intent2);
                finish();
            }
        });
        data = new WineDbHelper(getApplicationContext());

        if (data.fetchAllWines().getCount() < 1) { // si la bdd est vide on appelle populate pour la remplir
            data.populate();
        }

        refrech();


        // recup les infos du vin
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position); //recuperer l'item et le mettre dans un cursor

                Intent intent = new Intent(MainActivity.this, WineActivity.class); // Création de l'intent
                intent.putExtra("vin", data.cursorToWine(item));  //transformer le cursor en objet qu'on va envoyer à l'autre activité pour extraire tous les données
                startActivity(intent); // Envoyer l'intent qui contient toutes les infos
               // finish();
            }
        });

        registerForContextMenu(listView); // permet de savoir qur la liste est attaché a un menu
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Delete") {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor it = (Cursor) listView.getItemAtPosition(info.position); // pour conaitre la position de l'item qu'on veut supp

            data.deleteWine(it);
            refrech();
        }
        return true;
    }


    public void refrech(){
        Cursor result = data.fetchAllWines(); //recup toutes les lignes de la bdd on les met dans result
        result.moveToFirst();
        final Cursor curseur = data.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, result,
                new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, new int[]{android.R.id.text1, android.R.id.text2}, 0);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter); // adapter le resultat du curseur à la listeView
    }

    /*public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Cursor cur = data.fetchAllWines();
        cur.moveToPosition(info.position);
        int id = item.getItemId();
        if (id == R.id.supprimer) {
            //showMessage("Message","Suppression Succes");
            data.deleteBook(cur);
            adapter.changeCursor(data.getReadableDatabase().query(data.TABLE_NAME, new String[] {"ROWID AS _id",data.COLUMN__TITLE,data.COLUMN_AUTHORS},null, null, null, null, null));
            adapter.notifyDataSetChanged();
            return true;
        }

        return super.onContextItemSelected(item);
    }*/

}
